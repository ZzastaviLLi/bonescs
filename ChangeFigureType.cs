﻿using UnityEngine;
using System.Collections;
using System;
public class ChangeFigureType : MonoBehaviour {
	public BoneElementsContainer ElementContainer;
	public Int32 CurrentSkin = 0;
	// Use this for initialization
	public void ChangeSkin()
	{
		ElementContainer.Objects[Executor.OffsetFigure + Executor.CurrentFigureSkin].SetActive(false);
		CurrentSkin = Executor.OffsetFigure + Executor.CurrentFigureSkin;
		if (Executor.CurrentFigureSkin + 1 >= ElementContainer.NumberOfSkins [Executor.CurrentFigure]) {
			Executor.CurrentFigureSkin = 0;
		} 
		else
			Executor.CurrentFigureSkin++;
		ElementContainer.Objects[Executor.OffsetFigure + Executor.CurrentFigureSkin].SetActive(true);
	}
}
