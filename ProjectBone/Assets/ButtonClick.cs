﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
public class ButtonClick : MonoBehaviour {
	public BoneElementsContainer Container;
	public Boolean Increment;

	public void ChangeFigure()
	{
		Container.Objects [Executor.OffsetFigure + Executor.CurrentFigureSkin].SetActive (false);
		if (Increment) 
		{
			if (Executor.OffsetFigure + Container.NumberOfSkins[Executor.CurrentFigure] < Container.Objects.Length)
				Executor.OffsetFigure += Container.NumberOfSkins[Executor.CurrentFigure++];
			else 
			{
				Executor.OffsetFigure = 0;
				Executor.CurrentFigure = 0;
			}
		} 
		else 
		{
			if (Executor.CurrentFigure == 0)
			{
				Executor.CurrentFigure = Container.NumberOfSkins.Length - 1;
				Executor.OffsetFigure = Container.Objects.Length - Container.NumberOfSkins[Executor.CurrentFigure];
			}
			else
				Executor.OffsetFigure -= Container.NumberOfSkins[--Executor.CurrentFigure];
		}
		Executor.CurrentFigureSkin = 0;
		Container.Objects [Executor.OffsetFigure].SetActive (true);
	}
}
