﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
public class BoneObject<T> : IEnumerable<T>, IEquatable<BoneObject<T>> where T : class  {
	private List<T> objectTextures;

	public Int32 Length {get { return objectTextures.Count; }}

	public BoneObject() 
	{
		objectTextures = new List<T> ();
	}
	public BoneObject(IEnumerable<T> objects)
	{
		objectTextures = new List<T> (objects);
	}
	public T this[Int32 index] 
	{
		get 
		{
			if (index > -1 && index < objectTextures.Count) 
				return objectTextures[index];
			else return null;
		}
		set 
		{
			if (index > -1 && index < objectTextures.Count)
				objectTextures[index] = value;
		}
	}
	public IEnumerator<T> GetEnumerator()
	{
		foreach (var i in objectTextures)
			yield return i;
	}
	IEnumerator IEnumerable.GetEnumerator()
	{
		foreach (var i in objectTextures)
			yield return i;
	}
	public override bool Equals (object obj)
	{
		if (obj == null) return false;
		if (this.GetType () != obj.GetType ()) return false;
		if (!base.Equals (obj)) return false;
		BoneObject<T> bObj = obj as BoneObject<T>;
		if (this.Length == bObj.Length) {
			for (Int32 i = 0; i < objectTextures.Count; i++) {
				if (!this[i].Equals(bObj[i]))
					return false;
			}
		}
		return true;
	}
	public bool Equals (BoneObject<T> bObj)
	{
		if (this.Length == bObj.Length) {
			for (Int32 i = 0; i < objectTextures.Count; i++) {
				if (!this[i].Equals(bObj[i]))
					return false;
			}
		}
		return true;
	}
	public override int GetHashCode ()
	{
		return base.GetHashCode ();
	}
}
