﻿using UnityEngine;
using System.Collections;
using System;

public class BoneElement : MonoBehaviour {
	public Int32 MaxTouchesCount = 2;
	public Single RotationForce = .2f;
	public Single ScaleForce = 1.0f;
	public Single MinDeltaPosition = 4f;
	//сколько пользователь нажал/зажал пальцев на экране
	BitArray touchIDs;
	//touch delta in Vector2
	Single[] touchDeltas;
	Transform objTransform;
	/*
	 * В функции старт инициализируем значения стандартом
	 */
	void Start () 
	{
		touchIDs = new BitArray (MaxTouchesCount, false);
		touchDeltas = new[] {.0f, .0f};
	}
	void Update () 
	{
		objTransform = GetComponent<Transform> ();
		TouchFunction();
	}
	void TouchFunction()
	{
		Single scaleFromTouches = .0f; 
		foreach (Touch touch in Input.touches) 
		{
			switch (touch.phase)
			{
			case TouchPhase.Began:
				if (touch.fingerId < touchIDs.Length && !touchIDs[touch.fingerId])
					touchIDs[touch.fingerId] = true;
				//TouchPhaseBegin(touch);
				break;
			case TouchPhase.Ended:
				touchIDs[touch.fingerId] = false;
				//TouchPhaseEnded(touch);
				break;
			case TouchPhase.Moved:
				Single absX = Math.Abs (touch.deltaPosition.x);
				Single absY = Math.Abs (touch.deltaPosition.y);
				if (!touchIDs[1] && absX < MinDeltaPosition)
					transform.rotation = Quaternion.Euler(3,0,0);
				else if (!touchIDs[1] && absY < MinDeltaPosition)
					transform.rotation = Quaternion.Euler(0,3,0);
				else if (!touchIDs[1] && absY > MinDeltaPosition && absX > MinDeltaPosition)
					transform.rotation = Quaternion.Euler(0,0,5);
				else
				{
					scaleFromTouches += (Single)Math.Sqrt(Math.Pow (touch.position[0],2) + Math.Pow(touch.position[1],2))/2;
					transform.localScale += new Vector3(scaleFromTouches,scaleFromTouches,scaleFromTouches);
				}	
				break;
			default:
				break;
			}
		}
		if (touchIDs [1]) 
		{
			touchIDs[0] = false;
			touchIDs[1] = false;
		}
	}
	void TouchPhaseBegin(Touch touch)
	{
		if (touch.fingerId < touchIDs.Length && !touchIDs[touch.fingerId])
			touchIDs[touch.fingerId] = true;
		/*if (touchIDs[0] == false)
				touchIDs[0] = true;
			else if(touchIDs[1] == false)
				touchIDs[1] = true;*/
	}
	void TouchPhaseEnded(Touch touch)
	{
		touchIDs[touch.fingerId] = false;
	}
	void TouchPhaseMoved(Touch touch)
	{
		// эта функция должна имея значения длинны проведения пальцем менять значения дельт
		//функцию лучше изменить
		if (touch.fingerId < touchDeltas.Length)
			touchDeltas[touch.fingerId] += (touch.deltaPosition.x + touch.deltaPosition.y)/2; 
	}
}