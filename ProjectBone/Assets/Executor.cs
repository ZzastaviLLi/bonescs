﻿using UnityEngine;
using System.Collections;
using System;
public static class Executor  {
	public static Int32 CurrentFigure = 0;
	public static Int32 OffsetFigure = 0;
	public static Int32 CurrentFigureSkin = 0;
	public static String SavedOffset = "Offset";
	public static String SavedFigureSkin = "Skin";
	public static String SavedFigure = "Figure";

	public static void SaveAll()
	{
		PlayerPrefs.SetInt (Executor.SavedOffset, Executor.OffsetFigure);
		PlayerPrefs.SetInt (Executor.SavedFigureSkin, Executor.CurrentFigureSkin);
		PlayerPrefs.SetInt (Executor.SavedFigure, Executor.CurrentFigure);
		Application.Quit ();
	}
	public static Boolean GetAll()
	{
		if (PlayerPrefs.HasKey (SavedFigureSkin)) 
		{
			CurrentFigureSkin = PlayerPrefs.GetInt (SavedFigureSkin);
			CurrentFigure = PlayerPrefs.GetInt(SavedFigure);
			OffsetFigure = PlayerPrefs.GetInt(SavedOffset);
			return true;
		}
		return false;
	}
}
