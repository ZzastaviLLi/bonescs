﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
//using BOList = System.Collections.Generic.List<BoneObject<GameObject>>;
public class BoneElementsContainer : MonoBehaviour {

	public GameObject[] Objects;
	public Int32[] NumberOfSkins;
	// Use this for initialization
	void Start () {
		if (Executor.GetAll ()) 
		{
			for (Int32 i = 0; i < Objects.Length; i++) 
			{
				if (i == Executor.OffsetFigure + Executor.CurrentFigureSkin)
					Objects [i].SetActive (true);
				else 
					Objects [i].SetActive (false);
			}
		}
		else 
		{
			for (Int32 i = 1; i < Objects.Length; i++)
				Objects [i].SetActive (false);
		}
	}
}
