using UnityEngine;
using System.Collections;
using System.Threading;
using System;
public class BoneElement : MonoBehaviour {
	public Int32 MaxTouchesCount = 2;
	public Single RotationForce = .2f;
	public Single ScaleForce = 1.0f;
	public Single MinDeltaPosition = .2f;
	//сколько пользователь нажал/зажал пальцев на экране
	BitArray touchIDs;
	//touch delta in Vector2
	Single[] touchDeltas;
	Transform objTransform;
	/*
	 * В функции старт инициализируем значения стандартом
	 */
	void Start () 
	{
		touchIDs = new BitArray (MaxTouchesCount, false);
		touchDeltas = new[] {.0f, .0f};
	}
	void Update () 
	{
		objTransform = GetComponent<Transform> ();
		TouchFunction();
	}
	void TouchFunction()
	{
		Single scaleFromTouches = .0f; 
		foreach (Touch touch in Input.touches) 
		{
			switch (touch.phase)
			{
			case TouchPhase.Began:
				if (touch.fingerId < touchIDs.Length && !touchIDs[touch.fingerId])
					touchIDs[touch.fingerId] = true;
				//TouchPhaseBegin(touch);
				break;
			case TouchPhase.Ended:
				touchIDs[touch.fingerId] = false;
				//TouchPhaseEnded(touch);
				break;
			case TouchPhase.Moved:
				Single absX = Math.Abs (touch.deltaPosition.x);
				Single absY = Math.Abs (touch.deltaPosition.y);
				if (absX > MinDeltaPosition & absY > MinDeltaPosition & !touchIDs[1])
					objTransform.Rotate(new Vector3(0,0,((absX+absY)/2)*RotationForce));
				else if (absX > MinDeltaPosition & absY > MinDeltaPosition & touchIDs[1])
				{
					scaleFromTouches = ScaleForce * (absX + absY)/2;
					objTransform.localScale = new Vector3(scaleFromTouches,scaleFromTouches,scaleFromTouches);
				}
				else if (absX > MinDeltaPosition & absY <= MinDeltaPosition)
					objTransform.Rotate(new Vector3(0,absX/2*RotationForce,0));
				else if (absX <= MinDeltaPosition & absY > MinDeltaPosition)
					objTransform.Rotate(new Vector3(absX/2*RotationForce,0,0));
				//TouchPhaseMoved(touch);
				break;
			default:
				break;
			}
		}
	}
	void TouchPhaseBegin(Touch touch)
	{
		if (touch.fingerId < touchIDs.Length && !touchIDs[touch.fingerId])
			touchIDs[touch.fingerId] = true;
			/*if (touchIDs[0] == false)
				touchIDs[0] = true;
			else if(touchIDs[1] == false)
				touchIDs[1] = true;*/
	}
	void TouchPhaseEnded(Touch touch)
	{
		touchIDs[touch.fingerId] = false;
	}
	void TouchPhaseMoved(Touch touch)
	{
		// эта функция должна имея значения длинны проведения пальцем менять значения дельт
		//функцию лучше изменить
		if (touch.fingerId < touchDeltas.Length)
			touchDeltas[touch.fingerId] += (touch.deltaPosition.x + touch.deltaPosition.y)/2; 
	}
}